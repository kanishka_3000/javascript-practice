
function startTable(){
	return "<table id = \"tab_edit\">"
}

function endTable(){
	return "</table>"
}

function addTableRow(rowContent){
	var text = "<tr><td ed_table=\"true\">" + rowContent + "</td></tr>";
	return text;
}

function createTable(rows){
	
	var text = startTable();
	var size = rows.length;
	for(var i = 0; i < size; i++){
		var rowData = rows[i];
		text += addTableRow(rowData);
	}
	text += endTable();
	console.log(text);
	
	return text;
}

function getContentFromSingleColoumnRow(row){
	var colomn =  row.firstChild;
	var text = colomn.innerHTML;
	return text;
}

function createInputBox(inputData){
	var input = document.createElement("input");
	input.setAttribute("type", "text");
	input.value = inputData;
	
	return input;
}

function makeColumnsEditable(){
	var table = document.getElementById("tab_edit");
	if(table == null){
		console.log("tabedit not found");
	}
	var tBody = table.childNodes[0];
	var tabRows = tBody.childNodes;
	console.log("There are " + tabRows.length + "Rows");
	
	for(var i = 0 ;i < tabRows.length; i++){
		var row = tabRows[i];
		console.log("Iterating : " + i + row.nodeName);
		
		var rowData = getContentFromSingleColoumnRow(row);
		console.log("Row Data : " + rowData);
		
		var input = createInputBox(rowData);
		var colomn =  row.firstChild;
		colomn.innerHTML = "";
		
		colomn.appendChild(input);
	}
}