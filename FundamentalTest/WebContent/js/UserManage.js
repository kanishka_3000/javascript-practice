function findUserDetails(){
	
	var searchTerm = document.getElementById("search_term").value;
	
	var xmml = new XMLHttpRequest();
	xmml.onreadystatechange = onSearchReturn;
	xmml.open("GET", "UserHandler", true);
	xmml.send();
}

function onSearchReturn(){
	 if (this.readyState == 4 && this.status == 200) {
		 var resp = this.responseText;
		 populateWithUserInfo(resp);
		 
	 }
}

function populateWithUserInfo(user){
	
	var base = document.getElementById("dynamic_search");
	var users = JSON.parse(user);
	var user = users[1];
	var output = createTable([user.name, user.age]);
	
	base.innerHTML = output;
}
